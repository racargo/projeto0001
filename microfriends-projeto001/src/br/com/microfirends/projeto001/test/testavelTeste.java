package br.com.microfirends.projeto001.test;

import org.junit.Test;

import br.com.microfriends.projeto001.domain.Testavel;
import junit.framework.Assert;

public class testavelTeste {
	
	@Test
	public void deve_retornar_10_se_valor_maior_que_zero() {
		Testavel testavel = new Testavel();
		double valorRetornado = testavel.validarValor(1D);
		Assert.assertEquals(10D, valorRetornado);
		
	}

}
