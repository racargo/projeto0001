package br.com.microfriends.projeto001;

import java.util.Objects;
import java.util.Scanner;

import br.com.microfriends.projeto001.domain.Cliente;
import br.com.microfriends.projeto001.domain.FundoAcao;
import br.com.microfriends.projeto001.domain.FundoMultimercado;
import br.com.microfriends.projeto001.domain.FundoRendaFixa;
import br.com.microfriends.projeto001.service.ClienteService;
import br.com.microfriends.projeto001.service.FundoService;

public class Main {

	private static final ClienteService clienteService = new ClienteService();
	private static final FundoService fundoService = new FundoService(clienteService);
	private static final Scanner scanner = new Scanner(System.in);

	
	public static void main(String[] args) {
		while (true) {
			iniciarMenu();
		}
	}

	private static void iniciarMenu() {
		System.out.println(criarMenu());
		int opcao = scanner.nextInt();
		scanner.nextLine();

		switch (opcao) {
		case 1:
			cadastrarFundo();
			break;
		case 2:
			listarFundos();
			break;
		case 3:
			removerFundo();
			break;
		case 4:
			aplicarNoFundo();
			break;
		case 5:
			resgatarFundo();
			break;
		default:
			break;
		}
	}

	private static void cadastrarFundo() {
		try {
			System.out.println("#################");

			System.out.println("Qual o tipo do fundo ? ACAO (1) , MULTIMERCADO(2), RENDAFIXA(3)");
			int tipoFundo = scanner.nextInt();
			scanner.nextLine();

			System.out.println("Qual a descrição do fundo ?");
			String descricao = scanner.nextLine();

			System.out.println("Qual o valor da cota ?");
			Double valorCota = scanner.nextDouble();
			scanner.nextLine();

			System.out.println("Qual a quantidade de cotas disponíveis ?");
			int qtdCotas = scanner.nextInt();
			scanner.nextLine();

			System.out.println("Qual a quantidade mínima de cotas ?");
			int qtdMinCotas = scanner.nextInt();
			scanner.nextLine();

			switch (tipoFundo) {
			case 1:
				fundoService.criarFundo(new FundoAcao(descricao, valorCota, qtdCotas, qtdMinCotas));
				break;
			case 2:
				fundoService.criarFundo(new FundoMultimercado(descricao, valorCota, qtdCotas, qtdMinCotas));
				break;
			case 3:
				fundoService.criarFundo(new FundoRendaFixa(descricao, valorCota, qtdCotas, qtdMinCotas));
				break;
			default:
				break;
			}

			System.out.println("#################");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void removerFundo() {
		try {
			System.out.println("#################");

			System.out.println("Qual o código do fundo");
			String codigo = scanner.nextLine();
			fundoService.removerFundo(codigo);
			System.out.println("#################");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void aplicarNoFundo() {
		try {
			System.out.println("#################");
			System.out.println("Qual o CPF do cliente ?");
			String cpf = scanner.nextLine();
			Cliente cliente = clienteService.buscarCliente(cpf);

			if (Objects.isNull(cliente)) {
				System.out.println("Qual o Nome do cliente ?");
				String nome = scanner.nextLine();
				clienteService.cadastrarCliente(cpf, nome);
			}

			System.out.println("Qual o código do fundo ?");
			String codigoFundo = scanner.nextLine();
			
			System.out.println("Qual a quantidade de cotas desejadas ?");
			int quantidadeCotas = scanner.nextInt();
			scanner.nextLine();

			fundoService.aplicarNoFundo(cpf, codigoFundo, quantidadeCotas);
			System.out.println("#################");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void resgatarFundo() {
		try {
			System.out.println("#################");
			System.out.println("Qual o CPF do cliente ?");
			String cpf = scanner.nextLine();
			Cliente cliente = clienteService.buscarCliente(cpf);

			if (Objects.isNull(cliente)) {
				System.err.println("Cliente inexistente");
			} else {
				System.out.println("Qual o código do fundo ?");
				String codigoFundo = scanner.nextLine();

				System.out.println("Qual a quantidade de cotas para resgatar ?");
				int quantidadeCotas = scanner.nextInt();
				scanner.nextLine();

				fundoService.resgatarFundo(cpf, codigoFundo, quantidadeCotas);
			}

			System.out.println("#################");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void listarFundos() {
		fundoService.obterFundos().forEach(f -> {
			System.out.println("#################");
			System.out.println("Fundo " + f.getDescricao());
			System.out.println("Cod. " + f.getCodigo());
			System.out.println("Tipo " + f.getClass().getCanonicalName());
			System.out.println("Valor da Cota " + f.getValorCota());
			System.out.println("Qtd. Total " + f.getQuantidadeTotalCotas());
			System.out.println("Qtd. Disponível " + f.getQuantidadeDisponivelCotas());
			System.out.println("Clientes: ");
			f.getClientes().forEach(c -> {
				System.out.println(c.getCliente().getNome() + ", COTAS = " + c.getQuantidadeCotas());
			});
			System.out.println("#################");
		});
	}

	private static String criarMenu() {
		StringBuilder menu = new StringBuilder();
		menu.append("(1) Cadastrar novo fundo");
		menu.append("\n");
		menu.append("(2) Listar Fundos");
		menu.append("\n");
		menu.append("(3) Remover fundo");
		menu.append("\n");
		menu.append("(4) Aplicar no fundo");
		menu.append("\n");
		menu.append("(5) Resgatar do fundo");
		menu.append("\n");

		return menu.toString();
	}
	
	

}
