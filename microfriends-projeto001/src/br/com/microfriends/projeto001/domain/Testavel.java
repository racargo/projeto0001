package br.com.microfriends.projeto001.domain;

public class Testavel {

	public Double validarValor(Double valor) {
		if (valor > 0) {
			return 10D;
		} else {
			return valor * 2;
		}
	}
}
