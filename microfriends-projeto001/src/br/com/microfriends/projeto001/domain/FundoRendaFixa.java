package br.com.microfriends.projeto001.domain;

public class FundoRendaFixa extends Fundo {

	
	public FundoRendaFixa(String descricao, Double valorCota, Integer quantidadeDisponivelCotas,
			Integer quantidadeMinimaCotas) {
		super(descricao, valorCota, quantidadeDisponivelCotas, quantidadeMinimaCotas);
	}

}
