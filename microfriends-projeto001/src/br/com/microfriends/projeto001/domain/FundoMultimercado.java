package br.com.microfriends.projeto001.domain;

public class FundoMultimercado extends Fundo {

	
	public FundoMultimercado(String descricao, Double valorCota, Integer quantidadeDisponivelCotas,
			Integer quantidadeMinimaCotas) {
		super(descricao, valorCota, quantidadeDisponivelCotas, quantidadeMinimaCotas);
	}

}
