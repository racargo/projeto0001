package br.com.microfriends.projeto001.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Fundo extends EntidadeBase {

	private String descricao;
	private Double valorCota;
	private Integer quantidadeDisponivelCotas;
	private Integer quantidadeMinimaCotas;
	private Integer quantidadeTotalCotas = 0;
	private List<ClienteFundo> clientes = new ArrayList<>();

	
	public Fundo(String descricao, Double valorCota, Integer quantidadeTotalCotas, Integer quantidadeMinimaCotas) {
		super();
		this.descricao = descricao;
		this.valorCota = valorCota;
		this.quantidadeMinimaCotas = quantidadeMinimaCotas;
		this.quantidadeTotalCotas = quantidadeTotalCotas;
		this.quantidadeDisponivelCotas = quantidadeTotalCotas;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValorCota() {
		return valorCota;
	}

	public void setValorCota(Double valorCota) {
		this.valorCota = valorCota;
	}

	public Integer getQuantidadeDisponivelCotas() {
		return quantidadeDisponivelCotas;
	}

	public Integer getQuantidadeMinimaCotas() {
		return quantidadeMinimaCotas;
	}

	public void setQuantidadeMinimaCotas(Integer quantidadeMinimaCotas) {
		this.quantidadeMinimaCotas = quantidadeMinimaCotas;
	}

	public Integer getQuantidadeTotalCotas() {
		return quantidadeTotalCotas;
	}

	public void setQuantidadeTotalCotas(Integer quantidadeTotalCotas) {
		this.quantidadeTotalCotas = quantidadeTotalCotas;
	}

	public void adicionarCliente(Cliente cliente) {
		this.clientes.add(new ClienteFundo(this, cliente));
	}

	public void removerCliente(Cliente cliente) {
		this.clientes.remove(new ClienteFundo(this, cliente));
	}

	public List<ClienteFundo> getClientes() {
		return clientes;
	}

	public void adicionarCotaCliente(Cliente cliente, Integer quantidade) {
		ClienteFundo cf = clientes.stream().filter(c -> c.getCliente().equals(cliente)).findFirst().orElse(null);
		
		if (Objects.isNull(cf)) {
			cf = new ClienteFundo(this, cliente);
		}
		
		cf.setQuantidadeCotas(cf.getQuantidadeCotas() + quantidade);
		quantidadeDisponivelCotas = quantidadeDisponivelCotas - quantidade;
		this.clientes.add(cf);
	}

	public void removerCotaCliente(Cliente cliente, Integer quantidade) {
		ClienteFundo cf = clientes.stream().filter(c -> c.getCliente().equals(cliente)).findFirst().orElse(null);
		cf.setQuantidadeCotas(cf.getQuantidadeCotas() - quantidade);
		if (cf.getQuantidadeCotas() <= 0) {
			clientes.remove(cf);
			quantidadeDisponivelCotas = quantidadeDisponivelCotas + quantidade;
		}
	}

	@Override
	public String toString() {
		return "Fundo [codigo=" + codigo + ", descricao=" + descricao + "]";
	}

}
