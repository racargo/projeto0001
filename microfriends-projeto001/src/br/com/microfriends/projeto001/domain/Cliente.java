package br.com.microfriends.projeto001.domain;

public class Cliente extends EntidadeBase{

	private String cpf;
	private String nome;
	
	public Cliente(String cpf, String nome) {
		super();
		this.cpf = cpf;
		this.setNome(nome);
	}
	
	
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Cliente [cpf=" + cpf + ", nome=" + nome + "]";
	}

}
