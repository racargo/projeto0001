
package br.com.microfriends.projeto001.domain;

import java.util.UUID;

public abstract class EntidadeBase {

	protected UUID codigo;
	
	public EntidadeBase() {
		this.codigo = UUID.randomUUID();
	}

	
	public String getCodigo() {
		return codigo.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntidadeBase other = (EntidadeBase) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
	

}
