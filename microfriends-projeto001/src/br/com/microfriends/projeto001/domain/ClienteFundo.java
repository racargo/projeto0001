package br.com.microfriends.projeto001.domain;

public class ClienteFundo {

	private Fundo fundo;
	private Cliente cliente;
	private Double quantidadeCotas = 0D;
	
	public ClienteFundo(Fundo fundo, Cliente cliente) {
		super();
		this.fundo = fundo;
		this.cliente = cliente;
	}

	
	public Fundo getFundo() {
		return fundo;
	}

	public void setFundo(Fundo fundo) {
		this.fundo = fundo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Double getQuantidadeCotas() {
		return quantidadeCotas;
	}

	public void setQuantidadeCotas(Double quantidadeCotas) {
		this.quantidadeCotas = quantidadeCotas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((fundo == null) ? 0 : fundo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteFundo other = (ClienteFundo) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (fundo == null) {
			if (other.fundo != null)
				return false;
		} else if (!fundo.equals(other.fundo))
			return false;
		return true;
	}

}
