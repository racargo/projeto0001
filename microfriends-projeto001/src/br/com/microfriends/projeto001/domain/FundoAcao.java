package br.com.microfriends.projeto001.domain;

public class FundoAcao extends Fundo {

	
	public FundoAcao(String descricao, Double valorCota, Integer quantidadeDisponivelCotas,
			Integer quantidadeMinimaCotas) {
		super(descricao, valorCota, quantidadeDisponivelCotas, quantidadeMinimaCotas);
	}

}
