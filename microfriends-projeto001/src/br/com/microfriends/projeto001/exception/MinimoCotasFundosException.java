package br.com.microfriends.projeto001.exception;

public class MinimoCotasFundosException extends RuntimeException {

	
	public MinimoCotasFundosException(String message) {
		super(message);
	}

}
