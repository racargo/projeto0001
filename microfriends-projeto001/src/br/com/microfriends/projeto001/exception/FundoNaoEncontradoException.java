package br.com.microfriends.projeto001.exception;

public class FundoNaoEncontradoException extends RuntimeException {

	
	public FundoNaoEncontradoException(String message) {
		super(message);
	}

}
