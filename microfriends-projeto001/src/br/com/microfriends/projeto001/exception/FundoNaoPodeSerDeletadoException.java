package br.com.microfriends.projeto001.exception;

public class FundoNaoPodeSerDeletadoException extends RuntimeException {

	
	public FundoNaoPodeSerDeletadoException(String message) {
		super(message);
	}

}
