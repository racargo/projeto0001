package br.com.microfriends.projeto001.exception;

public class ClienteNaoEncontradoException extends RuntimeException {

	
	public ClienteNaoEncontradoException(String message) {
		super(message);
	}

}
