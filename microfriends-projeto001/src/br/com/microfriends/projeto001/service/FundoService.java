package br.com.microfriends.projeto001.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.microfriends.projeto001.domain.Cliente;
import br.com.microfriends.projeto001.domain.ClienteFundo;
import br.com.microfriends.projeto001.domain.Fundo;
import br.com.microfriends.projeto001.exception.ClienteNaoEncontradoException;
import br.com.microfriends.projeto001.exception.FundoNaoEncontradoException;
import br.com.microfriends.projeto001.exception.FundoNaoPodeSerDeletadoException;
import br.com.microfriends.projeto001.exception.MinimoCotasFundosException;
import br.com.microfriends.projeto001.exception.QtdCotasInsuficienteException;

public class FundoService {

	private List<Fundo> fundos = new ArrayList<>();
	private ClienteService clienteService;
	
	public FundoService(ClienteService clienteService) {
		super();
		this.clienteService = clienteService;
	}

	
	
	/**
	 * Obtem todos os fundos
	 * 
	 * @return List
	 */
	public List<Fundo> obterFundos() {
		return fundos;
	}

	/**
	 * Buscar por código
	 * 
	 * @param codigo
	 * @return {@link Fundo}
	 */
	public Fundo obterFundoPorCodigo(String codigo) {
		return fundos.stream().filter(f -> f.getCodigo().equals(codigo)).findFirst().orElse(null);
	}

	/**
	 * Cria fundo
	 * 
	 * @param fundo
	 */
	public void criarFundo(Fundo fundo) {
		fundos.add(fundo);
	}

	/**
	 * Remove fundo, se existir
	 * 
	 * @param codigo
	 */
	public boolean removerFundo(String codigo) {
		Fundo fundo = obterFundoPorCodigo(codigo);
		if (Objects.nonNull(fundo) && fundo.getClientes().size() > 0) {
			throw new FundoNaoPodeSerDeletadoException(
					"O fundo " + fundo.getDescricao() + " não pode ser deletado enquanto possuir clientes");
		}
		return fundos.remove(fundo);
	}

	/**
	 * Aplica no fundo para determinado cliente
	 * 
	 * @param cpf
	 * @param codigoFundo
	 */
	public void aplicarNoFundo(String cpf, String codigoFundo, Integer quantidadeCotas) {
		Fundo fundo = obterFundoPorCodigo(codigoFundo);
		if (Objects.isNull(fundo)) {
			throw new FundoNaoEncontradoException("Fundo de código " + codigoFundo + " não encontrado");
		}

		if (fundo.getQuantidadeMinimaCotas() > quantidadeCotas) {
			throw new MinimoCotasFundosException("Você não pode comprar menos que " + fundo.getQuantidadeMinimaCotas()
					+ " para o fundo " + fundo.getDescricao());
		}

		if (fundo.getQuantidadeDisponivelCotas() < quantidadeCotas) {
			throw new QtdCotasInsuficienteException("Só há " + fundo.getQuantidadeDisponivelCotas()
					+ " cotas disponíveis para o fundo " + fundo.getDescricao());
		}

		Cliente cliente = clienteService.buscarCliente(cpf);
		fundo.adicionarCotaCliente(cliente, quantidadeCotas);
	}

	/**
	 * Resgatar Fundo
	 * 
	 * @param cpf
	 * @param codigoFundo
	 * @param quantidadeCotas
	 */
	public void resgatarFundo(String cpf, String codigoFundo, Integer quantidadeCotas) {
		Fundo fundo = obterFundoPorCodigo(codigoFundo);
		if (Objects.isNull(fundo)) {
			throw new FundoNaoEncontradoException("Fundo de código " + codigoFundo + " não encontrado");
		}

		ClienteFundo cf = fundo.getClientes().stream().filter(c->c.getCliente().getCpf().equals(cpf)).findFirst().orElse(null);
		if (Objects.isNull(cf)) {
			throw new ClienteNaoEncontradoException("Cliente de CPF " + cpf + " não encontrado");
		}

		if (cf.getQuantidadeCotas() < quantidadeCotas) {
			throw new QtdCotasInsuficienteException(
					"Só há " + cf.getQuantidadeCotas() + " para retirar deste fundo para este cliente");
		}

		fundo.removerCotaCliente(cf.getCliente(), quantidadeCotas);
	}

}
