package br.com.microfriends.projeto001.service;

import java.util.ArrayList;
import java.util.List;

import br.com.microfriends.projeto001.domain.Cliente;

public class ClienteService {

	private List<Cliente> clientes = new ArrayList<>();

	
	
	/**
	 * Cadastra um novo cliente
	 * 
	 * @param cpf, nome
	 */
	public void cadastrarCliente(String cpf, String nome) {
		clientes.add(new Cliente(cpf, nome));
	}

	/**
	 * Buscar cliente
	 * 
	 * @param cpf
	 */
	public Cliente buscarCliente(String cpf) {
		return clientes.stream().filter(c -> c.getCpf().equals(cpf)).findFirst().orElse(null);
	}

}
